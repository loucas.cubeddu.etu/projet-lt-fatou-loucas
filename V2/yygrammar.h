#ifndef YYSTYPE
#define YYSTYPE long
#endif
extern YYSTYPE yylval;
extern long yypos;

#define SLASH 274
#define RCHEVRON 273
#define EQUAL 272
#define TARGET 271
#define EVENT 270
#define TRANSDEBUT 269
#define CHFALSE 268
#define CHTRUE 267
#define FINAL 266
#define ID 265
#define ETATFIN 264
#define ETATDEBUT 263
#define SCXMLFIN 262
#define CHAINE 261
#define INIT 260
#define REF 259
#define XMLNS 258
#define SCXMLDEBUT 257
#define PROLOGUE 256
