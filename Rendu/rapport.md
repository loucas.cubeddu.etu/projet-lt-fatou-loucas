# Version 1 : Analyse lexicale et syntaxique

## Avancement

Nous avons réalisé la première version du projet lors de la première séance. 
Nous disposons donc du code *lex* et *accent* pour l'analyseur lexical et syntaxique du projet.
Cet analyseur:
- Ignore les espaces et tabulations
- Compte les retours à la ligne (pour afficher la ligne où a lieu une eventuelle erreur de syntaxe)
On a codé la grammaire du projet dans *accent*.
Nous avons défini la syntaxe sur *lex*.

## Problèmes rencontrés

Nous avons rencontré de légers problèmes lors de la compilation.
La ligne:

```lex
\"({chiffre}|{lettre}|{tiret})*\"					return CHAINE;
```

avait été placée avant les lignes:

```lex
"\"true\""											return CHTRUE;
"\"false\""											return CHFALSE;
```

donc les deux lignes `CHTRUE` et `CHFALSE` n'étaient jamais reconnues par le Lexer, car `CHAINE` prenait la priorité dessus.

Une fois ce problème réglé nous sommes passés à la deuxième version.

\newpage

# Version 2 : Affichage

## Avancement

Nous avons réalisé l'affichage de l'automate lors de la première séance.
Cet affichage est fidèle à l'affichage demandé dans le sujet.

Il nous permet d'afficher:
- Quel état est l'état initial
- La liste des états avec:
    + Leur nom
    + La liste de leurs arcs
- Pour chaque arc:
    + Son étiquette
    + L'état d'arrivée de l'arc (successeur)

Nous sommes arrivés à ce résultat en affichant au fur et a mesure de l'exécution du Parser les différentes valeurs des symboles `CHAINE`.

**Liste des symboles**

- `CHAINE` $\rightarrow$ on utilise son attribut `valChaine` (défini dans `yystype.h`) pour afficher le nom des états et des etiquettes.

## Problèmes rencontrés

Nous sommes à un moment partis sur la mauvaise voie en essayant d'utiliser des attributs. 

Nous avions crus qu'ils étaient nécessaires après une erreur d'affichage qui était en fait facilement évitée.

Après avoir réarrangé l'ordre d'affichage, nous avons eu un programme fonctionnel.

\newpage

# Version 3 : Affichage suite

## Avancement

A la deuxième séance, nous avons un affichage terminé et conforme à la partie 3.

Il nous permet d'afficher en plus:

- Le nombre d'arcs de chaque état
- Si un état est final (en utilisant des *attributs synthétisés* à la place de la méthode précédente)
- Le nombre total d'états
- Le nombre d'états finaux
- Le nombre total d'arcs

Pour réaliser cette partie, nous avons utilisé des attributs synthétisés, que nous allons détailler ci-dessous.

**Liste des symboles**

- `ListeTransitions` a 1 attribut synthétisé:
    + `LT_nbarcsetat` qui compte le nombre de transitions, et par extension le nombre d'arcs.
- `AttFinal` a 1 attribut synthétisé:
    + `AF_etatfinal` qui est égal à 1 si l'état est final et 0 si l'état n'est pas final
- `Etat` a 2 attributs synthétisés:
    + `Etat_etatfinal` qui est égal à 1 si l'état est final et 0 si l'état n'est pas final (viens de `AttFinal`)
    + `Etat_nbarcsetat` qui compte le nombre de transitions, et par extension le nombre d'arcs dans l'état. (viens de `ListeTransitions`)
- `ListeEtats` a 3 attributs synthétisés:
    + `LE_nbetats` qui est le nombre total des états de l'automate
    + `LE_nbetatsfinaux` qui est le nombre total d'états finaux (utilise `Etat_etatfinal` pour savoir quels états compter)
    + `LE_nbarcstotal` qui est le nombre total d'arcs de l'automate (somme de tous les `Etat_nbarcsetat` de la liste d'états)

## Problèmes rencontrés

J'ai souvent oublié le `int` dans le 

```
<%out int ..., int ...>
```