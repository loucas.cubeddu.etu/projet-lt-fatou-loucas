#line 1 "scc.acc"
 /* Code C */
	/* Inclusion de bibliotheques C */
	#include <stdio.h>
	#include <malloc.h>
	
	/* Action de fin d analyse */
	void fin_analyse(){
		printf("Syntaxe correcte\n");
   }

# line 13 "yygrammar.c"
#include "yygrammar.h"

YYSTART ()
{
   switch(yyselect()) {
   case 14: {
      Sc();
      get_lexval();
      } break;
   }
}

Sc ()
{
   switch(yyselect()) {
   case 1: {
      get_lexval();
      get_lexval();
      get_lexval();
      get_lexval();
      get_lexval();
      get_lexval();
      get_lexval();
      get_lexval();
      get_lexval();
      ListeEtats();
      get_lexval();
#line 17 "scc.acc"
 fin_analyse(); 
# line 43 "yygrammar.c"
      } break;
   }
}

ListeEtats ()
{
   switch(yyselect()) {
   case 2: {
      Etat();
      ListeEtats();
      } break;
   case 3: {
      } break;
   }
}

Etat ()
{
   switch(yyselect()) {
   case 4: {
      get_lexval();
      AttId();
      AttFinal();
      get_lexval();
      ListeTransitions();
      get_lexval();
      } break;
   }
}

AttId ()
{
   switch(yyselect()) {
   case 5: {
      get_lexval();
      get_lexval();
      get_lexval();
      } break;
   }
}

AttFinal ()
{
   switch(yyselect()) {
   case 6: {
      get_lexval();
      get_lexval();
      get_lexval();
      } break;
   case 7: {
      get_lexval();
      get_lexval();
      get_lexval();
      } break;
   case 8: {
      } break;
   }
}

ListeTransitions ()
{
   switch(yyselect()) {
   case 9: {
      Trans();
      ListeTransitions();
      } break;
   case 10: {
      } break;
   }
}

Trans ()
{
   switch(yyselect()) {
   case 11: {
      get_lexval();
      TransEvent();
      TransTarget();
      get_lexval();
      get_lexval();
      } break;
   }
}

TransEvent ()
{
   switch(yyselect()) {
   case 12: {
      get_lexval();
      get_lexval();
      get_lexval();
      } break;
   }
}

TransTarget ()
{
   switch(yyselect()) {
   case 13: {
      get_lexval();
      get_lexval();
      get_lexval();
      } break;
   }
}

extern YYSTYPE yylval;
YYSTYPE yylval;
extern long yypos;
long yypos = 1;
/* GentleFlag = no */

typedef struct LEXELEMSTRUCT {
   YYSTYPE val;
   long pos;
   long sym;
   char * text;
   struct LEXELEMSTRUCT *next;
} LEXELEM;
   
LEXELEM *first_lexelem, *cur_lexelem;

init_lexelem()
{
   cur_lexelem = first_lexelem;
}

first_lexval () {
   LEXELEM *p;
   p = (LEXELEM *)malloc(sizeof(LEXELEM));
   if (! p) yymallocerror();
   p->val = yylval;
   p->pos = yypos;
   p->next = 0;
   cur_lexelem = p;
   first_lexelem = p;
}

next_lexval() {
   LEXELEM *p;
   p = (LEXELEM *)malloc(sizeof(LEXELEM));
   if (! p) yymallocerror();
   cur_lexelem-> next = p;
   p->val = yylval;
   p->pos = yypos;
   p->next = 0;
   cur_lexelem = p;
}

get_lexval() {
   extern int FREE_LEXELEMS;
   LEXELEM *p;
   yylval = cur_lexelem->val;
   yypos = cur_lexelem->pos;
   p = cur_lexelem;
   cur_lexelem = cur_lexelem->next;
   free(p);
}

extern int c_length;
int c_length = 85;
extern int yygrammar[];
int yygrammar[] = {
0,
/* 1 */ 0,
/* 2 */ 6,
/* 3 */ 50000,
/* 4 */ -1,
/* 5 */ 14,
/* 6 */ 0,
/* 7 */ 50256,
/* 8 */ 50257,
/* 9 */ 50258,
/* 10 */ 50272,
/* 11 */ 50259,
/* 12 */ 50260,
/* 13 */ 50272,
/* 14 */ 50261,
/* 15 */ 50273,
/* 16 */ 20,
/* 17 */ 50262,
/* 18 */ -6,
/* 19 */ 1,
/* 20 */ 25,
/* 21 */ 28,
/* 22 */ 20,
/* 23 */ -20,
/* 24 */ 2,
/* 25 */ 0,
/* 26 */ -20,
/* 27 */ 3,
/* 28 */ 0,
/* 29 */ 50263,
/* 30 */ 37,
/* 31 */ 43,
/* 32 */ 50273,
/* 33 */ 58,
/* 34 */ 50264,
/* 35 */ -28,
/* 36 */ 4,
/* 37 */ 0,
/* 38 */ 50265,
/* 39 */ 50272,
/* 40 */ 50261,
/* 41 */ -37,
/* 42 */ 5,
/* 43 */ 49,
/* 44 */ 50266,
/* 45 */ 50272,
/* 46 */ 50267,
/* 47 */ -43,
/* 48 */ 6,
/* 49 */ 55,
/* 50 */ 50266,
/* 51 */ 50272,
/* 52 */ 50268,
/* 53 */ -43,
/* 54 */ 7,
/* 55 */ 0,
/* 56 */ -43,
/* 57 */ 8,
/* 58 */ 63,
/* 59 */ 66,
/* 60 */ 58,
/* 61 */ -58,
/* 62 */ 9,
/* 63 */ 0,
/* 64 */ -58,
/* 65 */ 10,
/* 66 */ 0,
/* 67 */ 50269,
/* 68 */ 74,
/* 69 */ 80,
/* 70 */ 50274,
/* 71 */ 50273,
/* 72 */ -66,
/* 73 */ 11,
/* 74 */ 0,
/* 75 */ 50270,
/* 76 */ 50272,
/* 77 */ 50261,
/* 78 */ -74,
/* 79 */ 12,
/* 80 */ 0,
/* 81 */ 50271,
/* 82 */ 50272,
/* 83 */ 50261,
/* 84 */ -80,
/* 85 */ 13,
0
};
extern int yyannotation[];
int yyannotation[] = {
0,
/* 1 */ 0,
/* 2 */ 0,
/* 3 */ 50000,
/* 4 */ -1,
/* 5 */ 0,
/* 6 */ 0,
/* 7 */ 50256,
/* 8 */ 50257,
/* 9 */ 50258,
/* 10 */ 50272,
/* 11 */ 50259,
/* 12 */ 50260,
/* 13 */ 50272,
/* 14 */ 50261,
/* 15 */ 50273,
/* 16 */ 1,
/* 17 */ 50262,
/* 18 */ -6,
/* 19 */ 1,
/* 20 */ 25,
/* 21 */ 1,
/* 22 */ 1,
/* 23 */ -20,
/* 24 */ 1,
/* 25 */ 0,
/* 26 */ -20,
/* 27 */ 2,
/* 28 */ 0,
/* 29 */ 50263,
/* 30 */ 1,
/* 31 */ 1,
/* 32 */ 50273,
/* 33 */ 1,
/* 34 */ 50264,
/* 35 */ -28,
/* 36 */ 1,
/* 37 */ 0,
/* 38 */ 50265,
/* 39 */ 50272,
/* 40 */ 50261,
/* 41 */ -37,
/* 42 */ 1,
/* 43 */ 49,
/* 44 */ 50266,
/* 45 */ 50272,
/* 46 */ 50267,
/* 47 */ -43,
/* 48 */ 1,
/* 49 */ 55,
/* 50 */ 50266,
/* 51 */ 50272,
/* 52 */ 50268,
/* 53 */ -43,
/* 54 */ 2,
/* 55 */ 0,
/* 56 */ -43,
/* 57 */ 3,
/* 58 */ 63,
/* 59 */ 1,
/* 60 */ 1,
/* 61 */ -58,
/* 62 */ 1,
/* 63 */ 0,
/* 64 */ -58,
/* 65 */ 2,
/* 66 */ 0,
/* 67 */ 50269,
/* 68 */ 1,
/* 69 */ 1,
/* 70 */ 50274,
/* 71 */ 50273,
/* 72 */ -66,
/* 73 */ 1,
/* 74 */ 0,
/* 75 */ 50270,
/* 76 */ 50272,
/* 77 */ 50261,
/* 78 */ -74,
/* 79 */ 1,
/* 80 */ 0,
/* 81 */ 50271,
/* 82 */ 50272,
/* 83 */ 50261,
/* 84 */ -80,
/* 85 */ 1,
0
};
extern int yycoordinate[];
int yycoordinate[] = {
0,
/* 1 */ 9999,
/* 2 */ 17004,
/* 3 */ 9999,
/* 4 */ 9999,
/* 5 */ 17004,
/* 6 */ 9999,
/* 7 */ 9999,
/* 8 */ 9999,
/* 9 */ 9999,
/* 10 */ 9999,
/* 11 */ 9999,
/* 12 */ 9999,
/* 13 */ 9999,
/* 14 */ 9999,
/* 15 */ 9999,
/* 16 */ 17069,
/* 17 */ 9999,
/* 18 */ 9999,
/* 19 */ 17013,
/* 20 */ 9999,
/* 21 */ 20014,
/* 22 */ 20019,
/* 23 */ 9999,
/* 24 */ 20017,
/* 25 */ 9999,
/* 26 */ 9999,
/* 27 */ 21001,
/* 28 */ 9999,
/* 29 */ 9999,
/* 30 */ 23018,
/* 31 */ 23024,
/* 32 */ 9999,
/* 33 */ 23042,
/* 34 */ 9999,
/* 35 */ 9999,
/* 36 */ 23016,
/* 37 */ 9999,
/* 38 */ 9999,
/* 39 */ 9999,
/* 40 */ 9999,
/* 41 */ 9999,
/* 42 */ 26010,
/* 43 */ 9999,
/* 44 */ 9999,
/* 45 */ 9999,
/* 46 */ 9999,
/* 47 */ 9999,
/* 48 */ 29016,
/* 49 */ 9999,
/* 50 */ 9999,
/* 51 */ 9999,
/* 52 */ 9999,
/* 53 */ 9999,
/* 54 */ 30008,
/* 55 */ 9999,
/* 56 */ 9999,
/* 57 */ 31001,
/* 58 */ 9999,
/* 59 */ 33020,
/* 60 */ 33026,
/* 61 */ 9999,
/* 62 */ 33024,
/* 63 */ 9999,
/* 64 */ 9999,
/* 65 */ 34001,
/* 66 */ 9999,
/* 67 */ 9999,
/* 68 */ 36020,
/* 69 */ 36031,
/* 70 */ 9999,
/* 71 */ 9999,
/* 72 */ 9999,
/* 73 */ 36018,
/* 74 */ 9999,
/* 75 */ 9999,
/* 76 */ 9999,
/* 77 */ 9999,
/* 78 */ 9999,
/* 79 */ 39018,
/* 80 */ 9999,
/* 81 */ 9999,
/* 82 */ 9999,
/* 83 */ 9999,
/* 84 */ 9999,
/* 85 */ 42020,
0
};
/* only for BIGHASH (see art.c)
extern int DHITS[];
int DHITS[87];
*/
int TABLE[15][275];
init_dirsets() {
TABLE[14][256] = 1;
TABLE[1][256] = 1;
TABLE[2][263] = 1;
TABLE[3][262] = 1;
TABLE[4][263] = 1;
TABLE[5][265] = 1;
TABLE[6][266] = 1;
TABLE[7][266] = 1;
TABLE[8][273] = 1;
TABLE[9][269] = 1;
TABLE[10][264] = 1;
TABLE[11][269] = 1;
TABLE[12][270] = 1;
TABLE[13][271] = 1;
}

extern int yydirset();
int yydirset(i,j)
   int i,j;
{
   return TABLE[i][j];
}
int yytransparent(n)
   int n;
{
   switch(n) {
      case 1: return 0; break;
      case 6: return 0; break;
      case 20: return 1; break;
      case 28: return 0; break;
      case 37: return 0; break;
      case 43: return 1; break;
      case 58: return 1; break;
      case 66: return 0; break;
      case 74: return 0; break;
      case 80: return 0; break;
   }
}
char * yyprintname(n)
   int n;
{
   if (n <= 50000)
      switch(n) {
         case 1: return "YYSTART"; break;
         case 6: return "Sc"; break;
         case 20: return "ListeEtats"; break;
         case 28: return "Etat"; break;
         case 37: return "AttId"; break;
         case 43: return "AttFinal"; break;
         case 58: return "ListeTransitions"; break;
         case 66: return "Trans"; break;
         case 74: return "TransEvent"; break;
         case 80: return "TransTarget"; break;
   }
   else 
      switch(n-50000) {
         case 274: return "SLASH"; break;
         case 273: return "RCHEVRON"; break;
         case 272: return "EQUAL"; break;
         case 271: return "TARGET"; break;
         case 270: return "EVENT"; break;
         case 269: return "TRANSDEBUT"; break;
         case 268: return "CHFALSE"; break;
         case 267: return "CHTRUE"; break;
         case 266: return "FINAL"; break;
         case 265: return "ID"; break;
         case 264: return "ETATFIN"; break;
         case 263: return "ETATDEBUT"; break;
         case 262: return "SCXMLFIN"; break;
         case 261: return "CHAINE"; break;
         case 260: return "INIT"; break;
         case 259: return "REF"; break;
         case 258: return "XMLNS"; break;
         case 257: return "SCXMLDEBUT"; break;
         case 256: return "PROLOGUE"; break;
      }
   return "special_character";
}
