%{
#include "yygrammar.h"

char err[20]; /* Chaine de caracteres pour les erreurs de syntaxe */
%}

/* Definition de macros */
separateur		[ \t]
lettre			[a-zA-Z]
chiffre			[0-9]
tiret			[-_]

%%

"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"		return PROLOGUE;
"<scxml"											return SCXMLDEBUT;
"xmlns"												return XMLNS;
"\"http://www.w3.org/2005/07/scxml\""				return REF;
"initial"											return INIT;
"</scxml>"											return SCXMLFIN;
"<state"											return ETATDEBUT;
"</state>"											return ETATFIN;
"id"												return ID;
"final"												return FINAL;
"\"true\""											return CHTRUE;
"\"false\""											return CHFALSE;
\"({chiffre}|{lettre}|{tiret})*\"					return CHAINE;
"<transition"										return TRANSDEBUT;
"event"												return EVENT;
"target"											return TARGET;
">"													return RCHEVRON;
"/"													return SLASH;
"="													return EQUAL;
{separateur}+					; 					/* Elimination des espaces */
\n								yypos++;			/* Compte le nombre de lignes du fichier source */
.								{ sprintf(err,"Mauvais caractere %c",yytext[0]);
								  yyerror(err);		/* Generation d'une erreur de syntaxe */
								}

%%
